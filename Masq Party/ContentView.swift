import SwiftUI
import Vision
import CoreLocation
import AVFoundation


struct ContentView: View {
    @State var swipeIndex = 0
    @State var swipeOffset: CGSize = .zero

    @State var maskIndex = 2
    @State var masks: [Mask] = [
        Mask(
            id: UUID(),
            image: Image("mask_01"),
            texture: Image("texture_01"),
            seller_image: Image("seller_01"),
            seller: "Renee Hanks",
            title: "Face Mask With Filter \n" + "and Nose Wire",
            features: [
                "• Cotton layer outside",
                "• Non-woven membrane layer",
                "• Stainless steel nose wire"
            ],
            url: URL(string: "https://www.etsy.com/listing/799454527/face-mask-with-filter-and-nose-wire-non")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_02"),
            texture: Image("texture_02"),
            seller_image: Image("seller_02"),
            seller: "Donna Fear",
            title: "Batik Face Mask",
            features: [
                "• Triple lined",
                "• Soft wire at top",
                "• 1/8 inch elastic"
            ],
            url: URL(string: "https://www.etsy.com/listing/783615580/batik-face-mask-triple-lined-one-size")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_03"),
            texture: Image("texture_03"),
            seller_image: Image("seller_03"),
            seller: "Kwan",
            title: "Dark Blue Fabric Face \n" + "Mask",
            features: [
                "• Soft and breathable",
                "• Washable and reusable",
                "• Filter pocket"
            ],
            url: URL(string: "https://www.etsy.com/listing/779749656/cat-on-dark-blue-fabric-face-mask-3")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_04"),
            texture: Image("texture_04"),
            seller_image: Image("seller_04"),
            seller: "HandMadeMasks",
            title: "Handmade, Hand-Cut, \n" + "Hand-Sewn Mask",
            features: [
                "• Soft elastic ear loops",
                "• Built-in filter",
                "• Made in the USA"
            ],
            url: URL(string: "https://www.etsy.com/listing/800740509/2-for-20-dollars-includes-shipping-in")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_05"),
            texture: Image("texture_05"),
            seller_image: Image("seller_05"),
            seller: "Menique",
            title: "Protective Linen Face Mask",
            features: [
                "• Breathable linen material",
                "• Black outside, white inside",
                "• Anti-odor washable"
            ],
            url: URL(string: "https://www.etsy.com/listing/777953172/black-protective-reusable-face-mask-100")!
        )
    ]
    
    let extraMasks: [Mask] = [
        Mask(
            id: UUID(),
            image: Image("mask_06"),
            texture: Image("texture_06"),
            seller_image: Image("seller_06"),
            seller: "Everyday Headband",
            title: "Poly-Fabric Face Mask",
            features: [
                "• One size fits all",
                "• 8 inches x 5 inches",
                "• Cotton-Poly blend fabric"
            ],
            url: URL(string: "https://www.etsy.com/listing/795818147/filter-pocket-face-mask-cotton-poly")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_07"),
            texture: Image("texture_07"),
            seller_image: Image("seller_07"),
            seller: "Hailey C",
            title: "Reversible Cotton Face \n" + "Mask",
            features: [
                "• 3D shape to snugly fit face",
                "• Machine wash safe",
                "• 5 different variations"
            ],
            url: URL(string: "https://www.etsy.com/listing/798652871/reversible-5-colors-best-quality-cotton")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_08"),
            texture: Image("texture_08"),
            seller_image: Image("seller_08"),
            seller: "Ruffles & Reels",
            title: "Double Layer Mask With \n" + "Ties",
            features: [
                "• Ties around the head",
                "• Pocket for filter insert",
                "• 2 layers of cotton"
            ],
            url: URL(string: "https://www.etsy.com/listing/781362004/adult-face-mask-made-in-usa-100-cotton")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_09"),
            texture: Image("texture_09"),
            seller_image: Image("seller_09"),
            seller: "Tracy Smith",
            title: "Adjustable Nose Wire \n" + "Cotton Mask",
            features: [
                "• Made in the USA",
                "• Adjustable nose wire",
                "• Washable for re-use"
            ],
            url: URL(string: "https://www.etsy.com/listing/801700403/face-mask-cotton-facemask-adjustable")!
        ),
        Mask(
            id: UUID(),
            image: Image("mask_10"),
            texture: Image("texture_10"),
            seller_image: Image("seller_10"),
            seller: "Sonbol",
            title: "Dual Layer Handmade \n" + "Face Mask",
            features: [
                "• Eco-friendly fabric",
                "• Made in the USA",
                "• Unisex"
            ],
            url: URL(string: "https://www.etsy.com/listing/800735987/face-mask-hand-made-dual-layer-unisex")!
        )
    ]

    var dragAcross: some Gesture {
        DragGesture()
        .onChanged {
            value in
            self.swipeOffset.width = CGFloat(self.swipeIndex * 80) + value.translation.width
        }
        .onEnded {
            value in
            if value.translation.width > 94 {
                self.swipeIndex += 1
                self.masks.insert(self.extraMasks.shuffled().first!, at: 0)
            } else if value.translation.width < -94 {
                self.swipeIndex -= 1
                self.maskIndex += 1

                self.masks.append(self.extraMasks.shuffled().last!)
            }

            self.swipeOffset.width = CGFloat(self.swipeIndex * 80)
        }
    }

    @State var detailsOffset = CGSize(width: 0, height: 580)
    
    var dragDown: some Gesture {
        DragGesture()
        .onChanged {
            value in self.detailsOffset.height = CGFloat(
                max(100, 100 + value.translation.height)
            )
        }
        .onEnded {
            value in
            self.detailsOffset.height = CGFloat(value.translation.height > 100 ? 580 : 100)
        }
    }

    @ObservedObject var location = Location()
    @ObservedObject var faceDetection = FaceDetection()

    @State var cameraReady = AVCaptureDevice.authorizationStatus(for: .video) == .authorized


    var body: some View {
        ZStack {
            CameraPreview(status: self.cameraReady, results: self.faceDetection)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .blur(radius: self.detailsOffset.height == 580 ? 0 : 5)

            HStack {
                Image(systemName: "mappin.and.ellipse")
                Text(self.location.area)
            }
            .font(.footnote)
            .padding(10)
            .offset(y: -260)
            
            Path {
                path in
                path.move(to: CGPoint(x: self.faceDetection.maskRect.minX, y: self.faceDetection.maskRect.minY))
                path.addQuadCurve(
                    to: CGPoint(x: self.faceDetection.maskRect.maxX, y: self.faceDetection.maskRect.minY),
                    control: CGPoint(x: self.faceDetection.maskRect.midX, y: self.faceDetection.maskRect.minY - 60)
                )
                path.addLine(to: CGPoint(x: self.faceDetection.maskRect.maxX - 10, y: self.faceDetection.maskRect.maxY))
                path.addQuadCurve(
                    to: CGPoint(x: self.faceDetection.maskRect.minX + 10, y: self.faceDetection.maskRect.maxY),
                    control: CGPoint(x: self.faceDetection.maskRect.midX, y: self.faceDetection.maskRect.maxY + 80)
                )
                path.addLine(to: CGPoint(x: self.faceDetection.maskRect.minX, y: self.faceDetection.maskRect.minY))
            }
            .fill(ImagePaint(image: self.masks[self.maskIndex].texture, scale: 0.5))

            HStack(spacing: 20) {
                ForEach(self.masks) {
                    mask in
                    Rectangle()
                    .fill(ImagePaint(image: mask.image, scale: 0.25))
                    .frame(width: 140, height: 140)
                    .cornerRadius(15)
                    .offset(x: self.swipeOffset.width)
                }
            }
            .frame(width: 320)
            .offset(y: 200)
            .animation(.easeIn)
            .gesture(self.dragAcross)
            .onTapGesture {
                self.detailsOffset.height = CGFloat(self.detailsOffset.height == 100 ? 580 : 100)
            }

            ZStack(alignment: .bottom) {
                Rectangle()
                .fill(Color.white)
                .cornerRadius(15)
                .frame(height: 400)
                .shadow(radius: 10)

                VStack(spacing: 45) {
                    VStack(alignment: .leading, spacing: 10) {
                        Text(self.masks[self.maskIndex].title)
                        .bold()
                        .font(.title)
                        .foregroundColor(Color.black)

                        HStack(spacing: 10) {
                            VStack {
                                self.masks[self.maskIndex].seller_image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 50, height: 50)
                                .clipShape(Circle())

                                Text(self.masks[self.maskIndex].seller)
                                .bold()
                                .font(.footnote)
                                .foregroundColor(Color.black)
                            }

                            Image(systemName: "star.fill")
                            .foregroundColor(Color.purple)

                            Image(systemName: "star.fill")
                            .foregroundColor(Color.purple)
                    
                            Image(systemName: "star.fill")
                            .foregroundColor(Color.purple)
                    
                            Image(systemName: "star.fill")
                            .foregroundColor(Color.purple)
                    
                            Image(systemName: "star.fill")
                            .foregroundColor(Color.purple)
                        }
                    }
                    
                    VStack(alignment: .leading) {
                        Text(self.masks[self.maskIndex].features[0])
                        Text(self.masks[self.maskIndex].features[1])
                        Text(self.masks[self.maskIndex].features[2])
                    }
                    .font(.headline)
                    .foregroundColor(Color.black)

                    Button(action: {
                        UIApplication.shared.open(self.masks[self.maskIndex].url)
                    }) {
                        Text("Mask Up!")
                        .font(.largeTitle)
                        .bold()
                        .foregroundColor(Color.white)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 80)
                        .background(Color.purple)
                        .cornerRadius(10)
                    }
                }
                .padding(.bottom, 30)
            }
            .offset(y: self.detailsOffset.height)
            .animation(.easeIn)
            .gesture(self.dragDown)
        }
        .edgesIgnoringSafeArea(.all)
        .statusBar(hidden: true)
        .onAppear {
            if AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
                AVCaptureDevice.requestAccess(for: .video) {
                    accessGranted in self.cameraReady = accessGranted
                }
            } else {
                self.cameraReady = true
            }

            /*
            do {
                try handler.perform([request])
            } catch {
            }
            */
        }
    }
}


struct Mask: Identifiable {
    let id: UUID
    let image: Image
    let texture: Image
    let seller_image: Image
    let seller: String
    let title: String
    let features: [String]
    let url: URL
}


class Location: NSObject, ObservableObject, CLLocationManagerDelegate {
    let manager = CLLocationManager()
    @Published var area = "Local Area"

    override init() {
        super.init()

        let defaultArea = UserDefaults.standard.string(forKey: "area")

        if defaultArea != nil {
            self.area = defaultArea!
        } else {
            self.manager.delegate = self
            self.manager.requestWhenInUseAuthorization()
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }

        CLGeocoder().reverseGeocodeLocation(location) {
            places, error in
            guard error == nil else { return }
            self.area = places!.first!.locality!

            UserDefaults.standard.set(self.area, forKey: "area")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
}


struct CameraPreview: UIViewRepresentable {
    typealias UIViewType = CameraPreviewView
    
    var status = false
    var results: FaceDetection? = nil
    
    func makeUIView(
        context: UIViewRepresentableContext<CameraPreview>
    ) -> CameraPreviewView {
        return CameraPreviewView(cameraActive: status, detectionResults: results!)
    }
    
    func updateUIView(
        _ uiView: CameraPreviewView, context: UIViewRepresentableContext<CameraPreview>
    ) {
    }
}


class CameraPreviewView: UIView, AVCaptureVideoDataOutputSampleBufferDelegate {
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    var previewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
    
    var session: AVCaptureSession? = nil
    var detection: FaceDetection? = nil
    
    init(cameraActive: Bool, detectionResults: FaceDetection) {
        super.init(frame: .zero)
        
        if cameraActive {
            self.detection = detectionResults

            guard let videoDevice = AVCaptureDevice.default(
                .builtInWideAngleCamera, for: .video, position: .front
            ) else {
                return
            }

            guard let videoInput = try? AVCaptureDeviceInput(
                device: videoDevice
            ) else {
                return
            }

            let videoOutput = AVCaptureVideoDataOutput()
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoOutput.videoSettings = [
                (kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)
            ] as [String : Any]
            videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "video_frame_queue"))

            self.session = AVCaptureSession()
            self.session?.addInput(videoInput)
            self.session?.addOutput(videoOutput)
            
            guard let connection = videoOutput.connection(with: .video) else {
                return
            }
            connection.videoOrientation = .portrait
        }
    }
    
    required init?(coder viewCoder: NSCoder) {
        super.init(coder: viewCoder)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if self.session != nil {
            if self.superview != nil {
                self.previewLayer.session = self.session
                self.previewLayer.videoGravity = .resizeAspect
                self.session?.startRunning()
            } else {
                self.session?.stopRunning()
            }
        }
    }

    func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection) {
        guard let videoFrame = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        let request = VNDetectFaceRectanglesRequest() {
            detectionRequest, error in
            DispatchQueue.main.async {
                if error != nil {
                    return
                }

                guard let observations = detectionRequest.results as? [VNFaceObservation] else {
                    return
                }

                guard observations.count > 0 else {
                    return
                }
                
                let faceObservation = observations.first!
                let faceW = faceObservation.boundingBox.size.width * 320
                let faceH = faceObservation.boundingBox.size.height * 426.5
                let faceX = faceObservation.boundingBox.origin.x * 320
                let faceY = faceObservation.boundingBox.origin.y * 426.5

                let upperHalf = faceH * 0.75
                let lowerHalf = faceH * 0.25
            
                self.detection?.maskRect = CGRect(
                    x: faceX + (faceW * 0.05),
                    y: faceY + (upperHalf * 0.9),
                    width: faceW,
                    height: lowerHalf
                )
            }
        }

        let handler = VNImageRequestHandler(cvPixelBuffer: videoFrame)

        do {
            try handler.perform([request])
        } catch {
            print("ERROR!!!")
        }
    }
}


class FaceDetection: ObservableObject {
    @Published var maskRect: CGRect = CGRect(
        x: 73.70756816864014,
        y: 305.677444703877,
        width: 187.08242416381836,
        height: 83.11526872217655
    )
}
