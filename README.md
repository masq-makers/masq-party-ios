# Masq Party :performing_arts: :tada:

Help people find cloth face masks that match their personal style and reduce
the spread of COVID-19. A mobile app that uses face detection and the Etsy
search API to show how a mask looks on a shopper's face. The app directs
shoppers towards local makers to support artisans and small businesses in a
shopper's area.

## Get Started

```
$ git clone git@gitlab.com:masq-makers/masq-party-ios.git
$ cd masq-party-ios
$ brew install xcodegen
$ xcodegen generate
```

## Communication

[#masq-party](https://slack.com) on Slack

## Testing

* [TestFlight Beta Test App](https://apps.apple.com/us/app/testflight/id899247664)

## Development Resources

* [Apple Developer - SwiftUI](https://developer.apple.com/documentation/swiftui)
* [Apple Developer - Vision Framework](https://developer.apple.com/documentation/vision)
* [Apple Developer - Core Location](https://developer.apple.com/documentation/corelocation)
* [Etsy Developer - API](https://etsy.com/developers/documentation)

